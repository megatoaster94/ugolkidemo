/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "GameScene.h"

USING_NS_CC;

bool g_vsAIGS = false;

Scene *GameScene::createScene(bool vsAI)
{
    g_vsAIGS = vsAI;
    return GameScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in GameSceneScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //set background color to grey
    LayerColor *bgColor = LayerColor::create(Color4B::GRAY);
    this->addChild(bgColor , -10);


    m_vsAI = g_vsAIGS;

    bool player_as_white = RandomHelper::random_int(0, 1);

    std::string str_enemy = m_vsAI ? "AI" : "player";
    std::string player_as_white_str = player_as_white ? "white" : "black";
    std::string msg_part = m_vsAI ? "\nYou play as " + player_as_white_str : "";
    auto label = Label::createWithTTF("Playing player vs " + str_enemy + msg_part, "fonts/Marker Felt.ttf", 24);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }

    m_board.prepareChips(player_as_white);

    m_board_node = m_board.setupNode();
    this->addChild(m_board_node);
    m_board_node->setPositionNormalized(Vec2(0.5, 0.5));

    m_board.placeChips();

    _keyboard_listener = EventListenerKeyboard::create();
    _keyboard_listener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
    _keyboard_listener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_keyboard_listener, this);

    this->setMouseListenersEnabled();

    return true;
}

void GameScene::gameEnded(bool player_win)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    std::string msg_part = player_win ? "WIN!" : "LOSE!";
    auto label = Label::createWithTTF("YOU " + msg_part, "fonts/Marker Felt.ttf", 48);
    label->setColor(Color3B::BLACK);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }
    scheduleOnce(CC_SCHEDULE_SELECTOR(GameScene::popScene), 3);
}

void GameScene::popScene(float)
{
    Director::getInstance()->popScene();
}


void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
    switch(keyCode){
    case EventKeyboard::KeyCode::KEY_ESCAPE:
        if (m_is_move) {
            m_is_move = false;
            m_board.cancelMove();
            break;
        }
        Director::getInstance()->popScene();
        break;
    case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
    case EventKeyboard::KeyCode::KEY_A:
        break;
    case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
    case EventKeyboard::KeyCode::KEY_D:
        break;
    case EventKeyboard::KeyCode::KEY_UP_ARROW:
    case EventKeyboard::KeyCode::KEY_W:
        break;
    case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
    case EventKeyboard::KeyCode::KEY_S:
        break;
    case EventKeyboard::KeyCode::KEY_SPACE:
        break;
    }
}

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event)
{

}

void GameScene::onMouseDown(Event *event)
{
    EventMouse* e = (EventMouse*)event;
    EventMouse::MouseButton mouse_button = e->getMouseButton();
    Vec2 mouse_position = e->getLocationInView();

    if (mouse_button == EventMouse::MouseButton::BUTTON_LEFT) {
        if (m_is_move) {
            bool successful_turn = m_board.makeMove(mouse_position);
            if (successful_turn) {
                m_is_move = false;
                m_is_players_turn = false;
                //Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
                this->setMouseListenersEnabled(false);
                if (bool player_win = m_board.checkWin()) {
                    this->gameEnded(player_win);
                }
                else {
                    scheduleOnce(CC_SCHEDULE_SELECTOR(GameScene::botTurn), 0.1);
                }
            }
            return;
        }
        if (m_is_players_turn) {
            if (m_board.chipSelected(mouse_position)) {
                m_is_move = true;
            }
        }
    }
}

void GameScene::onMouseUp(Event *event)
{
    EventMouse* e = (EventMouse*)event;
    EventMouse::MouseButton mouse_button = e->getMouseButton();
}

void GameScene::onMouseMove(Event *event)
{
    EventMouse* e = (EventMouse*)event;
    Vec2 mouse_position = Vec2(e->getCursorX(), e->getCursorY());
}

void GameScene::onMouseScroll(Event *event)
{
    EventMouse* e = (EventMouse*)event;
    Vec2 mouse_position = Vec2(e->getCursorX(), e->getCursorY());
    Vec2 mouse_scroll = Vec2(e->getScrollX(), e->getScrollY());
}

void GameScene::setMouseListenersEnabled(bool enabled)
{
    if (enabled) {
        _mouse_listener = EventListenerMouse::create();
        _mouse_listener->onMouseMove = CC_CALLBACK_1(GameScene::onMouseMove, this);
        _mouse_listener->onMouseUp = CC_CALLBACK_1(GameScene::onMouseUp, this);
        _mouse_listener->onMouseDown = CC_CALLBACK_1(GameScene::onMouseDown, this);
        _mouse_listener->onMouseScroll = CC_CALLBACK_1(GameScene::onMouseScroll, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_mouse_listener, this);
    }
    else {
        _eventDispatcher->removeEventListener(_mouse_listener);
        _mouse_listener->release();
        _mouse_listener = nullptr;
    }
}

void GameScene::botTurn(float)
{
    if (m_board.AIturn()) {
        if (bool bot_win = m_board.checkWin(false)) {
            this->gameEnded(!bot_win);
        }
        else {
            m_is_players_turn = true;
            this->setMouseListenersEnabled();
            //Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
        }
    }
}


void GameScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}
