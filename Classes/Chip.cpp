#include "Chip.h"

Chip::Chip()
{

}

Chip::Chip(ChipColor color) :
    m_color(color)
{

}

Chip::~Chip()
{

}

Node *Chip::setupNode(bool white)
{
    std::string w_or_b_str = white ? "W" : "B";
    auto node = m_node = Sprite::create("chess/pawn" + w_or_b_str + "2.png");
    return node;
}

Vec2 Chip::pos() const
{
    return m_pos;
}

void Chip::setPos(const Vec2 &pos)
{
    m_pos = pos;
}

ChipColor Chip::color() const
{
    return m_color;
}

Node *Chip::node() const
{
    return m_node;
}
