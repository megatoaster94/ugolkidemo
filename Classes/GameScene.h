/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"

#include "Board.h"

class GameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene(bool vsAI);

    virtual bool init();

    void gameEnded(bool player_win);
    void popScene(float);

    virtual void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

    virtual void onMouseDown(cocos2d::Event *event);
    virtual void onMouseUp(cocos2d::Event *event);
    virtual void onMouseMove(cocos2d::Event *event);
    virtual void onMouseScroll(cocos2d::Event *event);

    //enable/disable mouse listeners
    void setMouseListenersEnabled(bool enabled = true);

    //is bot's turn callback
    void botTurn(float);
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
private:
    EventListenerMouse *_mouse_listener = nullptr;
    EventListenerKeyboard *_keyboard_listener = nullptr;

    bool m_vsAI = false;
    bool m_is_players_turn = true;
    bool m_is_move = false; //if player's turn

    Board m_board;
    Node* m_board_node;
};

#endif // __GameScene_SCENE_H__
