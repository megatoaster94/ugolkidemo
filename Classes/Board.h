#ifndef __BOARD_H__
#define __BOARD_H__

#include <vector>
#include <iostream>

#include "cocos2d.h"

#include "Chip.h"

#define BOARD_SIZE_ROWS 8
#define BOARD_SIZE_COLS 8

#define HOUSE_SIZE_ROWS 3
#define HOUSE_SIZE_COLS 3

USING_NS_CC;

enum class CellState { EMPTY, WHITE, BLACK };

class Board
{
public:
    Board();
    ~Board();

    bool prepareChips(bool player_as_white = true);
    Node* setupNode();
    bool placeChips();

    Node* getCellSprite(Vec2 position);
    bool chipSelected(Vec2 position);
    bool makeMove(Vec2 position);
    void cancelMove();

    bool AIturn();

    void incTurnCount();
    int turn_count() const;
    bool checkWin(bool is_player = true);

    Node *node() const;

    std::vector<Chip> chips() const;

private:
    Board(Board &oth);

    const Size m_house_size = Size(HOUSE_SIZE_ROWS, HOUSE_SIZE_COLS);

    const std::vector<Vec2> m_positions = {Vec2(5, 0), Vec2(6, 0), Vec2(7, 0), Vec2(5, 1), Vec2(6, 1), Vec2(7, 1), Vec2(5, 2), Vec2(6, 2), Vec2(7, 2),
                                         Vec2(0, 5), Vec2(1, 5), Vec2(2, 5), Vec2(0, 6), Vec2(1, 6), Vec2(2, 6), Vec2(0, 7), Vec2(1, 7), Vec2(2, 7)};

    Vec2 getCellPos(Vec2 mouse_position);

    std::vector<Vec2> m_available_moves = {};
    std::array<std::array<bool, BOARD_SIZE_COLS>, BOARD_SIZE_ROWS> m_visited = {{{false}}};
    std::vector<Vec2> getAvailableMoves(Vec2 chip_pos);
    std::vector<Vec2> getAvailableJumps(Vec2 chip_pos);
    bool checkMoveInBounds(Vec2 move);

    std::pair<Vec2, Vec2> calculateBestMove();
    void possibleMove(Vec2 from, Vec2 to);
    float alphaBetaPrunning(int depth, float alpha, float beta, bool is_bot = true);
    float getHeuristicEvaluation(bool is_player);
    void botMoveCalculated(Vec2 from, Vec2 to);

    //calculate points (all chip distances to opponent's house), less is better
    float calculatePoints(bool is_player = true);
    float distanceToOpponentHouse(Vec2 chip_pos, bool is_player = true);

    int chipsInHouse(bool is_player, bool in_own);

    std::array<std::array<CellState, BOARD_SIZE_COLS>, BOARD_SIZE_ROWS> m_model = {{{CellState::EMPTY}}};
    void updateModel();
    void resetCellsColor();

    bool m_player_as_white = true;

    int m_turn_count = 0;

    std::vector<Chip> m_chips;
    std::pair<size_t, size_t> pickChips(bool white = true);
    size_t m_selected_chip = 0;

    Node *m_node = nullptr;
};

#endif
