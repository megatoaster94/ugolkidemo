#include "Board.h"

Board::Board()
{
    m_chips.insert(m_chips.end(), 9, Chip(ChipColor::WHITE));
    m_chips.insert(m_chips.end(), 9, Chip(ChipColor::BLACK));
}

Board::~Board()
{

}

bool Board::prepareChips(bool player_as_white)
{
    m_player_as_white = player_as_white;
    size_t chips_size = m_chips.size();
    if (player_as_white) {
        for (size_t i = 0; i < chips_size; ++i) {
            m_chips[i].setPos(m_positions[i]);
        }
    }
    else {
        for (size_t i = 0; i < chips_size / 2; ++i) {
            m_chips[i].setPos(m_positions[i + chips_size / 2]);
        }
        for (size_t i = chips_size / 2; i < chips_size; ++i) {
            m_chips[i].setPos(m_positions[i - chips_size / 2]);
        }
    }

    this->updateModel();

    return true;
}

Node *Board::setupNode()
{
    auto node = m_node = Sprite::create("chess/board.png");

    Vec2 zero_position(31.5, 31.5);
    for (size_t row = 0; row < BOARD_SIZE_ROWS; ++row) {
        for (size_t col = 0; col < BOARD_SIZE_COLS; ++col) {
            std::string b_or_w_str = (row + col) % 2 == 0 ? "B" : "W";
            auto square = Sprite::create("chess/square" + b_or_w_str + ".png");
            auto squareSize = square->getContentSize();
            square->setPosition(zero_position + Vec2((squareSize.width + 0.7) * col, (squareSize.height + 0.7) * row));
            node->addChild(square, 0, std::to_string(col) + std::to_string(row));
        }
    }

    return node;
}

bool Board::placeChips()
{
    if (!m_node) {
        log("Setup node first!");
        return false;
    }
    for (auto &chip : m_chips) {
        Vec2 chip_pos = chip.pos();
        auto chip_color = chip.color();
        auto chip_node = chip.setupNode(chip_color == ChipColor::WHITE);
        if (chip_node) {
            const auto &cell_node = m_node->getChildByName(std::to_string((int)chip_pos.x) + std::to_string((int)chip_pos.y));
            auto cell_node_position = cell_node->getPosition();
            m_node->addChild(chip_node);
            chip_node->setPosition(cell_node_position);
        }
        else {
            return false;
        }
    }
    this->updateModel();
    return true;
}

Node *Board::getCellSprite(Vec2 position)
{
    if (!m_node->getBoundingBox().containsPoint(m_node->getParent()->convertToNodeSpace(position))) {
        log("Outside node");
        return nullptr;
    }
    for (auto node : m_node->getChildren()) {
        Vec2 mouse_position_node_space = node->getParent()->convertToNodeSpace(position);
        if (node->getBoundingBox().containsPoint(mouse_position_node_space)) {
            return node;
        }
    }
    return nullptr;
}

bool Board::chipSelected(Vec2 position)
{
    /*for (size_t row = 0; row < BOARD_SIZE_ROWS; ++row) {
        for (size_t col = 0; col < BOARD_SIZE_COLS; ++col) {
            std::cout << (m_model[row][col] != CellState::EMPTY ? "1" : "0");
        }
        std::cout << std::endl;
    }*/

    auto cell_pos = this->getCellPos(position);

    //log("Cell pos = %fx%f", cell_pos.x, cell_pos.y);

    std::pair<size_t, size_t> section = this->pickChips(m_player_as_white);
    auto first = m_chips.cbegin() + section.first;
    auto last = m_chips.cbegin() + section.second;

    const auto &chip = std::find_if(first, last, [=] (const Chip& chip) { return chip.pos().equals(cell_pos); });

    if (chip != last) {
        //log("Chip pos = %fx%f", chip->pos().x, chip->pos().y);

        m_selected_chip = chip - m_chips.begin();

        auto available_moves = m_available_moves = this->getAvailableMoves((*chip).pos());

        for (auto move : available_moves) {
            auto move_node = m_node->getChildByName(std::to_string((int)move.x) + std::to_string((int)move.y));
            move_node->setColor(Color3B::GREEN);
        }

        if (available_moves.size() > 0) {
            return true;
        }
    }

    return false;
}

bool Board::makeMove(Vec2 position)
{
    auto selected_pos = this->getCellPos(position);

    const auto &move_pos = std::find_if(m_available_moves.begin(),
                                        m_available_moves.end(),
                                        [=] (const Vec2& pos) { return pos.equals(selected_pos); });

    if (move_pos != m_available_moves.end()) {
        Chip &selected_chip = m_chips[m_selected_chip];
        selected_chip.setPos(*move_pos);

        auto chip_node = selected_chip.node();

        std::string cell_name = std::to_string((int)(*move_pos).x) + std::to_string((int)(*move_pos).y);
        chip_node->setPosition(m_node->getChildByName(cell_name)->getPosition());

        this->updateModel();
        this->cancelMove();

        return true;
    }

    return false;
}

void Board::possibleMove(Vec2 from, Vec2 to)
{
    const auto &chip = std::find_if(m_chips.begin(), m_chips.end(), [=] (const Chip& chip) { return chip.pos().equals(from); });

    if (chip != m_chips.end()) {
        size_t chip_index = chip - m_chips.begin();
        m_chips[chip_index].setPos(to);
        this->updateModel();
    }
}

void Board::botMoveCalculated(Vec2 from, Vec2 to)
{
    const auto &chip = std::find_if(m_chips.begin(), m_chips.end(), [=] (const Chip& chip) { return chip.pos().equals(from); });

    if (chip != m_chips.end()) {
        size_t chip_index = chip - m_chips.begin();
        Chip &selected_chip = m_chips[chip_index];

        selected_chip.setPos(to);

        auto chip_node = selected_chip.node();
        std::string cell_name = std::to_string((int)to.x) + std::to_string((int)to.y);
        chip_node->setPosition(m_node->getChildByName(cell_name)->getPosition());

        this->updateModel();
    }
}

void Board::cancelMove()
{
    this->resetCellsColor();
    m_available_moves.clear();
}

bool Board::AIturn()
{
    std::pair<Vec2, Vec2> move = this->calculateBestMove();

    log("Calculated move = from { %fx%f }, to { %fx%f }", move.first.x, move.first.y, move.second.x, move.second.y);

    this->botMoveCalculated(move.first, move.second);
    this->incTurnCount();

    return true;
}

std::pair<Vec2, Vec2> Board::calculateBestMove()
{
    std::pair<size_t, size_t> section = this->pickChips(!m_player_as_white);
    auto first = m_chips.cbegin() + section.first;
    auto last = m_chips.cbegin() + section.second;

    float rating = -INFINITY;
    std::pair<Vec2, Vec2> result = {Vec2::ZERO, Vec2::ZERO};

    //save model
    const auto saved_model = m_model;
    const auto saved_chips = m_chips;

    for (auto it = first; it < last; ++it) {
        Vec2 old_pos = it->pos();
        std::vector<Vec2> available_moves = this->getAvailableMoves((*it).pos());
        for (auto move : available_moves) {
            //make move
            this->possibleMove(old_pos, move);

            float new_rating = this->alphaBetaPrunning(3, -INFINITY, INFINITY);
            //log("New rating = %f", new_rating);
            if (new_rating > rating) {
                rating = new_rating;
                result = std::make_pair(old_pos, move);
            }

            //restore model
            m_model = saved_model;
            m_chips = saved_chips;
        }
    }

    return result;
}

float Board::alphaBetaPrunning(int depth, float alpha, float beta, bool is_bot)
{
    if (depth == 0 ||
        checkWin(true) || //player win
        checkWin(false))  //bot win
    {
        return this->getHeuristicEvaluation(false) -
               this->getHeuristicEvaluation(true) / (m_turn_count + 1);
    }

    bool pick_white = m_player_as_white != is_bot;
    std::pair<size_t, size_t> section = this->pickChips(pick_white);
    auto first = m_chips.cbegin() + section.first;
    auto last = m_chips.cbegin() + section.second;

    //save model
    const auto saved_model = m_model;
    const auto saved_chips = m_chips;

    for (auto it = first; it < last; ++it) {
        auto available_moves = this->getAvailableMoves((*it).pos());

        for (auto move : available_moves) {
            //make move
            this->possibleMove(it->pos(), move);

            float gamma = this->alphaBetaPrunning(depth - 1, alpha, beta, !is_bot);

            if (is_bot) {
                if (gamma < beta)
                    beta = gamma;
            }
            else {
                if (gamma > alpha)
                    alpha = gamma;
            }

            //restore model
            m_model = saved_model;
            m_chips = saved_chips;

            if (beta <= alpha)
                break;
        }
    }

    return is_bot ? beta : alpha;
}

//honestly stole
float Board::getHeuristicEvaluation(bool is_player)
{
    float evaluation = 0;

    int chips_in_own_house = this->chipsInHouse(is_player, true);

    float distance_to_opponent_house = this->calculatePoints(is_player);
    int chips_in_opponent_house = this->chipsInHouse(is_player, false);

    //evaluation += chips_in_opponent_house * 10000.0;
    evaluation -= distance_to_opponent_house;
    //evaluation -= chips_in_own_house * 10000.0;

    /*double distanceToOpponentsHouse = this->calculatePoints(is_player); //negative criterion
    int piecesInOpponentsHouse = this->chipsInHouse(is_player, false); //positive criterion
    int piecesInOwnHouse = this->chipsInHouse(is_player, true); //positive criterion
    double housePunishment = 0; //negative criterion

    double grade = 3;
    distanceToOpponentsHouse = pow(distanceToOpponentsHouse, grade);

    if (piecesInOpponentsHouse == m_house_size.width * m_house_size.height)
        piecesInOpponentsHouse += 2000000;

    evaluation += distanceToOpponentsHouse * 100.0;
    grade = 3;
    housePunishment -= piecesInOwnHouse * pow(m_turn_count, grade) + evaluation * m_turn_count * m_turn_count;

    evaluation += piecesInOpponentsHouse * 150000.0;
    evaluation += housePunishment * 0.000000003;*/

    return evaluation;
}

void Board::incTurnCount()
{
    m_turn_count++;
}

int Board::turn_count() const
{
    return m_turn_count;
}

bool Board::checkWin(bool is_player)
{
    return chipsInHouse(is_player, false) == m_house_size.width * m_house_size.height;
}

Node *Board::node() const
{
    return m_node;
}

std::vector<Chip> Board::chips() const
{
    return m_chips;
}

Vec2 Board::getCellPos(Vec2 mouse_position)
{
    for (auto node : m_node->getChildren()) {
        Vec2 mouse_position_node_space = m_node->convertToNodeSpace(mouse_position);
        if (node->getBoundingBox().containsPoint(mouse_position_node_space)) {
            std::string node_name = node->getName();
            if (!node_name.empty()) {
                int x = std::stoi(node_name.substr(0, 1));
                int y = std::stoi(node_name.substr(1));

                return Vec2(x, y);
            }
        }
    }
    return Vec2(-1, -1);
}

void Board::updateModel()
{
    m_model = {{{CellState::EMPTY}}};

    for (auto chip : m_chips) {
        Vec2 chip_pos = chip.pos();
        auto chip_color = chip.color();
        m_model[(int)chip_pos.x][(int)chip_pos.y] = (chip_color == ChipColor::WHITE) ? CellState::WHITE : CellState::BLACK;
    }
}

void Board::resetCellsColor()
{
    for (auto node : m_node->getChildren()) {
        std::string node_name = node->getName();
        if (!node_name.empty()) {
            node->setColor(Color3B::WHITE);
        }
    }
}

std::pair<size_t, size_t> Board::pickChips(bool white)
{
    /*const auto middle = m_chips.size() / 2;
    auto first = white ? m_chips.begin() : m_chips.begin() + middle;
    auto last = white ? m_chips.begin() + middle : m_chips.end();*/
    const auto middle = m_chips.size() / 2;
    auto first = m_chips.cbegin();
    if (!white)
        first += middle;
    auto last = m_chips.cend();
    if (white)
        last -= middle;

    size_t first_index = first - m_chips.cbegin();
    size_t last_index = last - m_chips.cbegin();

    return std::make_pair(first_index, last_index);
}

std::vector<Vec2> Board::getAvailableMoves(Vec2 chip_pos)
{
    std::vector<Vec2> available_moves;

    m_visited = {{{false}}};

    for (Vec2 move : {Vec2(chip_pos.x, chip_pos.y + 1),
                      Vec2(chip_pos.x, chip_pos.y - 1),
                      Vec2(chip_pos.x + 1, chip_pos.y),
                      Vec2(chip_pos.x - 1, chip_pos.y)})
    {
        if (this->checkMoveInBounds(move) &&
            m_model[move.x][move.y] == CellState::EMPTY)
        {
            available_moves.push_back(move);
            m_visited[move.x][move.y] = true;
        }
    }

    std::vector<Vec2> available_jumps = this->getAvailableJumps(chip_pos);
    if (available_jumps.size() > 0)
        available_moves.insert(available_moves.end(), available_jumps.begin(), available_jumps.end());

    return available_moves;
}

std::vector<Vec2> Board::getAvailableJumps(Vec2 chip_pos)
{
    std::vector<Vec2> available_jumps;

    m_visited[chip_pos.x][chip_pos.y] = true;

    //                                                our_move(cell empty)              cell not empty
    for (std::pair<Vec2, Vec2> move : {std::make_pair(Vec2(chip_pos.x, chip_pos.y + 2), Vec2(chip_pos.x, chip_pos.y + 1)),
                                       std::make_pair(Vec2(chip_pos.x, chip_pos.y - 2), Vec2(chip_pos.x, chip_pos.y - 1)),
                                       std::make_pair(Vec2(chip_pos.x + 2, chip_pos.y), Vec2(chip_pos.x + 1, chip_pos.y)),
                                       std::make_pair(Vec2(chip_pos.x - 2, chip_pos.y), Vec2(chip_pos.x - 1, chip_pos.y))})
    {
        if (!m_visited[move.first.x][move.first.y] &&
            this->checkMoveInBounds(move.first) &&
            m_model[move.second.x][move.second.y] != CellState::EMPTY &&
            m_model[move.first.x][move.first.y] == CellState::EMPTY)
        {
            available_jumps.push_back(move.first);
            m_visited[move.first.x][move.first.y] = true;
            auto more_jumps = this->getAvailableJumps(move.first);
            available_jumps.insert(available_jumps.end(), more_jumps.begin(), more_jumps.end());
        }
    }

    return available_jumps;
}

bool Board::checkMoveInBounds(Vec2 move)
{
    if (move.x >= 0 && move.x < BOARD_SIZE_ROWS &&
        move.y >= 0 && move.y < BOARD_SIZE_COLS) {
        return true;
    }
    return false;
}

float Board::calculatePoints(bool is_player)
{
    bool check_white = m_player_as_white == is_player;

    std::pair<size_t, size_t> section = this->pickChips(check_white);
    auto first = m_chips.cbegin() + section.first;
    auto last = m_chips.cbegin() + section.second;

    float points = 0;

    for (auto it = first; it < last; ++it) {
        points += this->distanceToOpponentHouse((*it).pos(), is_player);
    }

    return points;
}

float Board::distanceToOpponentHouse(Vec2 chip_pos, bool is_player)
{
    return chip_pos.distance(is_player ?
                             Vec2(0, BOARD_SIZE_ROWS - 1) :
                             Vec2(BOARD_SIZE_COLS - 1, 0));
}

int Board::chipsInHouse(bool is_player, bool in_own)
{
    int result = 0;

    bool check_white = m_player_as_white == is_player;
    std::pair<size_t, size_t> section = this->pickChips(check_white);
    auto first = m_chips.cbegin() + section.first;
    auto last = m_chips.cbegin() + section.second;

    Vec2 pattern = is_player != in_own ?
                   Vec2(0, 7) :
                   Vec2(7, 0);

    for (auto it = first; it < last; ++it) {
        if (std::fabs((*it).pos().x - pattern.x) < m_house_size.width &&
            std::fabs((*it).pos().y - pattern.y) < m_house_size.height)
        {
            result++;
        }
    }

    return result;
}






