#ifndef __CHIP_H__
#define __CHIP_H__

#include "cocos2d.h"

USING_NS_CC;

enum class ChipColor { WHITE, BLACK };

class Chip
{
public:
    Chip();
    Chip(ChipColor color);
    ~Chip();

    Node* setupNode(bool white);

    Vec2 pos() const;
    void setPos(const Vec2 &pos);

    ChipColor color() const;

    Node *node() const;

private:
    Vec2 m_pos = {0, 0};

    ChipColor m_color = ChipColor::WHITE;

    Node *m_node = nullptr;
};

#endif
